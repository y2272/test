class Patient {
  constructor(name, sex, nation, birth, idcardnum, address, phone, contactman, contactman_phone, date, base_sickness, else_sickness){
    this.name = name
    this.sex = sex
    this.nation = nation
    this.birth = birth
    this.idcardnum = idcardnum
    this.address = address
    this.phone = phone
    this.contactman = contactman
    this.contactman_phone = contactman_phone
    this.date = date
    this.base_sickness = base_sickness
    this.else_sickness = else_sickness
  }
};
export {Patient};