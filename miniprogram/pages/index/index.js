const app = getApp()
Page({
    // 获取用户信息
    // 获取用户信息
    getUser(e){
      var flag = 0
      //获得用户的头像和昵称
      wx.getUserProfile({
        desc: '获得用户信息'
 
      }).then((res) => {
          console.log(res.userInfo.avatarUrl,res.userInfo.nickName)
          // 将内容赋值给全局的userInfo，这样可以在别的页面中使用
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo:res.userInfo
          })
          // 判断yuyue-user数据库中是否存在原用户，不存在则添加到数据库中，存在则替换
          wx.cloud.database().collection('yuyue-user').where({
            _openid:app.globalData.openid
          }).get().then(result=>{
            console.log(result)
            if(result.data.length === 0){
              // 添加到数据库用户列表中 yuyue-user
              wx.cloud.database().collection('yuyue-user').add({
                data:{
                  // 添加一个号码，由于id和openid太长，此号码可作为唯一标识
                  num: Date.now(),
                  // 添加用户昵称和头像
                  nickName:res.userInfo.nickName,
                  avatarUrl:res.userInfo.avatarUrl
                
                }
              }).then(res=>{
                  console.log(res)
                  // 添加成功提示
                  wx.showToast({
                    title: '登录成功！'
                  })
                    console.log(res._openid)
                    wx.cloud.database().collection('Doctor').where({
                      Doctorid : res._openid
                    }).get().then(result=>{
                      //如果是医生
                      if(result.length!=0){
                        wx.switchTab({
                          url: '/pages/homeDoctor/homeDoctor',
                        })
                    }else{//如果是患者
                        wx.switchTab({
                          url: '/pages/homePatient/homePatient',
                        })
                    } 
                    })           
                 }).catch(err=>{
                  console.log(err)
                })
              }else{
                this.setData({
                 userInfo:result.data[0]
                })
              }
          })
          
         
        } )
    }
  
})