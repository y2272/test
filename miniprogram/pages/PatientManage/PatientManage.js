Page({
  data: {
    name : "",
    sex : "",
    nation : "",
    birth : "",
    idcardnum : "",
    address : "",
    phone : "",
    contactman : "",
    contactman_phone : "",
    date : "",
    base_sickness : "",
    else_sickness : " ",
    url : ""
  },

  onLoad: function (options) {
    var name = options.name
    var id = options.id
    name = name.replace(/'/g,'')
    id = id.replace(/'/g,'')
    this.setData({
      name : name,
    })
    wx.cloud.database().collection('Patinet').where({
      Patientid : id
    }).get()
    .then(res =>{
      var sex = res.data[0].sex
      var nation = res.data[0].nation
      var birth = res.data[0].birth
      var idcardnum = res.data[0].IDcard_id
      var adr = res.data[0].address
      var phone = res.data[0].phone
      var peo = res.data[0].emecontact
      var peo_phone = res.data[0].emecontact_phone
      var url = res.data[0].url
      this.setData({
        sex : sex,
        nation : nation,
        birth : birth,
        idcardnum : idcardnum,
        address : adr,
        phone : phone,
        contactman : peo,
        contactman_phone : peo_phone,
        url : url
      })
    })
    wx.cloud.database().collection('Record').where({
      Patientid : id
    }).get()
    .then(res =>{
      var date = res.data[0].visit_date
      var b_sick = res.data[0].basic_sickness
      var e_sick = res.data[0].else_sickness
      this.setData({
        date : date,
        base_sickness : b_sick,
        else_sickness : e_sick
      })
    })
  }
})