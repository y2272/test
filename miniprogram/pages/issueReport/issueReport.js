// pages/issueReport/issueReport.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        eye_left:"",
        eye_right:"",
        patientname: "王为" ,
        underlyingillness: "糖尿病",
        medicalhistory:"青光眼",
        Previoussurgery:"眼角膜激光矫正",
        leftai:"左眼结果",
        rightai:"右眼结果",
        Machinemodel:"佳能",
        leftvisiontype:"裸眼视力",
        leftvision:"4.5",
        rightvisiontype:"裸眼视力",
        rightvision:"4.8",
        firstsuggestion:"请选择",
        elsesuggestion:"其他",

        Machinemodearray: ['佳能', '蔡司'],
        Lefteyearray:['裸眼','矫正视力','自带镜视力'],
        Righteyearray:['裸眼','矫正视力','自带镜视力'],
        suggestarray:['根据眼底图像结果显示：正常眼底，建议定期随访','根据眼底图像结果显示：有AMD可能，建议至眼科进一步筛查'],
        currentChooseMachine: 0,
        currentChooseLefteye: 0,
        currentChooseRighteye: 0,
        currentChoosesuggest:0
    },

    addone(options) {
      let db = wx.cloud.database()
  let userCollection = db.collection('Doctor')
  userCollection.where({
   //先查询
   Name:'罗辑'
  }).update({
   data: {
    todayNum: this.data.todayNum + 1,
    thisMonthNum:  this.data.thisMonthNum + 1,
      }
  }).then(res => {
   console.log('更新成功')
   this.setData({
    todayNum: this.data.todayNum + 1,
    thisMonthNum: this.data.thisMonthNum + 1
   })
  }).catch(err => {
   console.log('更新失败',err)//失败提示错误信息
  })
   },
   
    bindPickerChange: function (e) {
      console.log('picker发送选择改变，携带值为', e.detail.value)
      this.setData({
        currentChooseMachine: e.detail.value
      })
    },
    bindPickerChange1: function (e1) {
      console.log('picker发送选择改变，携带值为', e1.detail.value)
      this.setData({
        currentChoosesuggest: e1.detail.value
      })
    },
    bindPickerChange2: function (e2) {
      console.log('picker发送选择改变，携带值为', e2.detail.value)
      this.setData({
        currentChooseLefteye: e2.detail.value
      })
    },
    bindPickerChange3: function (e3) {
      console.log('picker发送选择改变，携带值为', e3.detail.value)
      this.setData({
        currentChooseRighteye: e3.detail.value
      })
    },
    /**
   * 输入框实时回调
   * @param {*} options 
   */
  remarkInputAction: function (options) {
    //获取输入框输入的内容
    let value = options.detail.value;
    console.log("输入框输入的内容是 " + value)
  },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        var left = options.lefteye
        var right = options.righteye
        var name = options.name
        this.setData({
          eye_left: left,
          eye_right:right,
          patientname:name
        })
        this.bindPickerChange(e)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})