Page({
  data:{
    name : [],
    id : [],
    list : [],
  },
  input(e){
    this.search(e.detail.value)
  },
  search(key){
        var that=this;
        if(key == ''){
          this.setData({
            List : []
          })
          return;
        }
        var arr=[];  
        var name = that.data.name;
        var id = that.data.id;  
        for(var i = 0; i < name.length; i ++){  
          if (name[i].indexOf(key)>=0 || id[i].indexOf(key)>=0){ 
            arr.push({"name":name[i]})
          }
        }
        if(arr.length==0){
          that.setData({
            List:[{name:'暂未查到此人'}]
          })
        }else{
          that.setData({
            List: arr
          })
        }
      },
  //
  
  getmessage: function(word,leng,i){
    if(i == leng) return;
    wx.cloud.database().collection('Patinet').where({
      initial : word[i]
    }).get()
    .then(res =>{
      this.getmessage(word,leng,i+1)
      var message = this.data.list
      message.push({"letter":res.data[0].initial,"data":res.data})
      this.setData({
        list : message
      })
    })
  },

  onLoad: function (options) {
    var that = this;
    var ini = [];
    wx.cloud.database().collection('Patinet').get()
    //成功
    .then(res =>{
      var name_l = [];
      var id_l = [];
      for(var i = 0; i < res.data.length; i ++){
        var letter = res.data[i].initial;
        var name = res.data[i].name;
        var id = res.data[i].Patientid;
        name_l.push(name);
        id_l.push(id);
        if(ini.indexOf(letter) == -1) ini.push(letter);
        ini.sort();
      }
        that.getmessage(ini,ini.length,0)
        this.setData({
          name : name_l,
          id : id_l,
        })
      })
    //失败
    .catch(err =>{
      console.log("失败",err)
    })
  },

})