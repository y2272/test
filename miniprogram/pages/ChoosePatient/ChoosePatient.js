var list = [
  {"letter":"Q", 
  "data":[{"name":"钱布朵","id":"1"}]},
  {"letter":"Y",
  "data":[{"name":"颜淡","id":"2"},
          {"name":"叶梓萱","id":"3"}]},
  {"letter":"#",
  "data":[{"name":"1彭海荣","id":"4"}]},
  ]
  var l = ["钱布朵","颜淡","叶梓萱","1彭海荣"]
Page({
  data:{
    userData: list
  },
  input(e){
    this.search(e.detail.value)
  },
  search(key){
    var that=this;
    var List=wx.getStorage({
      key: 'List',
      success: function(res) {
        if(key==''){ 
          that.setData({
            List:res.data
          })
          return;
        }
        var arr=[];    
        for(let i in res.data){  
          if (res.data[i].indexOf(key)>=0){ 
            arr.push({"name":res.data[i]})
          }
        }
        if(arr.length==0){
          that.setData({
            List:[{name:'暂未查到此人'}]
          })
        }else{
          that.setData({
            List: arr
          })
        }
      },
    })
  },
  onLoad: function (options) {
    var List= l
    wx.setStorage({
      key: 'List',
      data: List
    })
    this.setData({
      List:List
    })
  },
})