// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db = cloud.database();
// 云函数入口函数
exports.main = async (event, context) => {
    try{
      return await db.collection('Doctor').where({
        Name:'罗辑'
      }).update({
        data: {
          thisMonthNum: 0,
        }
      }).then(res => {
        console.log('更新成功')
        this.setData({
         thisMonthNum: 0,
        })
       }).catch(err => {
        console.log('更新失败',err)
       })
      }
      catch(e){
        console.log(e)
      }
}
 