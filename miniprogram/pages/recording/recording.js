// pages/recording/recording.js
const app = getApp()
Page({
  //   data: { List:[{
  //     number:"182378192649",
  //     name:"汤姆",
  //     time:"2022-10-10",
  //   },{
  //     number:"182378192649",
  //     name:"汤姆",
  //     time:"2022-10-11",
  //   },{
  //     number:"182378192649",
  //     name:"汤姆",
  //     time:"2022-10-12",
  //   },{
  //     number:"182378192649",
  //     name: "蔡徐坤",
  //     time:"2022-10-13",
  //   }
  // ],
  //     list: [{'name':'汤姆'},{'name':'蔡徐坤'}],
  //     list2: [{'name':'汤姆'},{'name':'蔡徐坤'}],
  //     focus:false,
  //     inputValue:""
  //   },
  data:{
    Recordid:[],
    name:[],
    time:[],
    list:[]
  },  
   input(e){
    this.search(e.detail.value)
  },
  search(key){
        var that=this;
        if(key == ''){
          this.setData({
            List : []
          })
          return;
        }
        var arr=[];  
        var name = that.data.name;
        var Recordid = that.data.Recordid;  
        for(var i = 0; i < name.length; i ++){  
          if (name[i].indexOf(key)>=0 || Recordid[i].indexOf(key)>=0){ 
            arr.push({"name":name[i]})
          }
        }
        if(arr.length==0){
          that.setData({
            List:[{name:'暂未查到此人'}]
          })
        }else{
          that.setData({
            List: arr
          })
        }
      },

  focusHandler(e){
      this.setData({focus:true});
    },
    cancelHandler(e)
    {
      this.setData({focus:false});
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.cloud.database().collection('Record').where({
      // Patientid:app.globalData.openid
      Patientid:"HZ4596123587"
    }).get().then(res=>{
      this.setData({
        list: res.data
      })
      console.log("res",this.data.list[0].time.toLocaleString())
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})