// pages/homeDoctor/homeDoctor.js
const app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Name: "",
    todayNum: "",
    thisMonthNum: "",
    usertype1:0
},

  treatMent:function(){
    wx.navigateTo({
      url: '/pages/recording/recording',
    })
  },
  eyePhoto:function(){
    wx.navigateTo({
      url: '/pages/eye/eye',
    })
  },
  result:function(){
    wx.navigateTo({
      url: '/pages/issueReport/issueReport',
     
    })
   
  }, 
  /**
  * 生命周期函数--监听页面显示
  */
 onShow: function(){
  this.setData({
  usertype1: getApp().globalData.userType,
})

 },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log("home页面用户类别："+ getApp().globalData.userType)
    wx.cloud.database().collection('Doctor').where({
      Name:'罗辑'
    }).get()
    .then(res =>{
      var Name =res.data[0].Name
      var todayNum =res.data[0].todayNum
      var thisMonthNum =res.data[0].thisMonthNum
      this.setData({
        Name:Name,
        todayNum:todayNum,
        thisMonthNum:thisMonthNum
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

 

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
