// pages/quit/quit.js
const app = getApp()
Page({
  // 退出登录
  /**
   * 页面的初始数据
   */

  exitUser(){
    // 全局和页面上的用户信息为空
     console.log(app.globalData.userInfo)
    wx.cloud.database().collection('yuyue-user').where({
      _openid: app.globalData.openid
    }).remove()
    .then(res=>{
      console.log("删除成功")
    }).catch(err=>{
      console.log("删除失败")
    })
    app.globalData.userInfo = null
    this.setData({
      userInfo: null
    })
    wx.reLaunch({
      url:'/pages/index/index'
    })
  },

  data: {
    l : []
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.cloud.database().collection('yuyue-user').where({
      _openid:app.globalData.openid
    }).get()
    //成功
    .then(res =>{
      console.log(res)
      this.setData({
        l : res.data
      })
    })
    //失败
    .catch(err =>{
      console.log("失败",err)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
class Patient {
  constructor(name, sex, nation, birth, idcardnum, address, phone, contactman, contactman_phone, date, base_sickness, else_sickness){
    this.name = name
    this.sex = sex
    this.nation = nation
    this.birth = birth
    this.idcardnum = idcardnum
    this.address = address
    this.phone = phone
    this.contactman = contactman
    this.contactman_phone = contactman_phone
    this.date = date
    this.base_sickness = base_sickness
    this.else_sickness = else_sickness
  }
};
export {Patient};