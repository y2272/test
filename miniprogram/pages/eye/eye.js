// pages/eye/eye.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        patientid:"330106111111111111",
        patientname:"王为",
        eye_left:"",
        eye_right:""
    },

    doUploadl(){
      // 选择上传的图片
      var thisd = this
      wx.chooseImage({
        count: 1,            //这里是选择几张图片
        sizeType: ['compressed'],   //把上传的图片进行压缩图
        sourceType: ['album', 'camera'],    //选择相册或拍照
        success: function (res) {
          wx.showLoading({
            title: '上传中',
          })
          const filePath = res.tempFilePaths[0]
          // 上传图片
          const cloudPath = 'my-image' + Date.now() + filePath.match(/\.[^.]+?$/)[0]   //上传图片         的名字，根据时间自定义图片文件名，使得上传后的每一张文件名都不一样，放置重名覆盖。
          wx.cloud.uploadFile({    //将图片上传到云服务器的云存储中
            cloudPath, //云存储的路径
            filePath,  //本地图片路径
            success: res => {
              console.log('[上传文件] 成功：', res) 
              console.log(res.fileID)  // 上传后图片的 fileID
              thisd.setData({
                eye_left: res.fileID
              })
              console.log(eye_left)
              // wx.navigateTo({   上传成功后跳转到某个页面
              //   url: '../storageConsole/storageConsole'
              // })
            },
            fail: e => {
              console.error('[上传文件] 失败：', e)
              wx.showToast({
                icon: 'none',
                title: '上传失败',
              })
            },
            complete: () => {
              wx.hideLoading()
            }
          })
  
        },
        fail: e => {
          console.error(e)
        }
      })
    },

    doUploadr(){
      // 选择上传的图片
      var thisd = this
      wx.chooseImage({
        count: 1,            //这里是选择几张图片
        sizeType: ['compressed'],   //把上传的图片进行压缩图
        sourceType: ['album', 'camera'],    //选择相册或拍照
        success: function (res) {
          wx.showLoading({
            title: '上传中',
          })
          const filePath = res.tempFilePaths[0]
          // 上传图片
          const cloudPath = 'my-image' + Date.now() + filePath.match(/\.[^.]+?$/)[0]   //上传图片         的名字，根据时间自定义图片文件名，使得上传后的每一张文件名都不一样，放置重名覆盖。
          wx.cloud.uploadFile({    //将图片上传到云服务器的云存储中
            cloudPath, //云存储的路径
            filePath,  //本地图片路径
            success: res => {
              console.log('[上传文件] 成功：', res) 
              console.log(res.fileID)  // 上传后图片的 fileID
              thisd.setData({
                eye_right: res.fileID
              })
              console.log(eye_right)
              // wx.navigateTo({   上传成功后跳转到某个页面
              //   url: '../storageConsole/storageConsole'
              // })
            },
            fail: e => {
              console.error('[上传文件] 失败：', e)
              wx.showToast({
                icon: 'none',
                title: '上传失败',
              })
            },
            complete: () => {
              wx.hideLoading()
            }
          })
  
        },
        fail: e => {
          console.error(e)
        }
      })
    },
  
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})