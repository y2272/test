Page({
  data: {
  Recordid:"",
  model:"",
  hos_name:"",
  adm_name:"",
  staff_name:"",
  pat_name:"",
  left_type:"",
  left_deg:"",
  right_type:"",
  right_deg:"",
  left_pic:"",
  left_ai:"",
  right_pic:"",
  right_ai:"",
  sgt:"",
  else_sgt:"",
  bsc_sck:"",
  else_sck:""
  },

  onLoad: function (options){
    var id = options.Recordid
    id = id.replace(/'/g,'')
    this.setData({
      Recordid:id
    })
    wx.cloud.database().collection('Record').where({
      Recordid : id
    }).get()
    .then(res =>{
      var model = res.data[0].mod
      var hos_name = res.data[0].hos_name
      var adm_name = res.data[0].adm_name
      var staff_name = res.data[0].doc_name
      var pat_name = res.data[0].name
      var left_type = res.data[0].left_type
      var left_deg = res.data[0].left_deg
      var left_url = res.data[0].left_url
      var left_ai = res.data[0].left_ai
      var right_type = res.data[0].right_type
      var right_deg = res.data[0].right_deg
      var right_url = res.data[0].right_url
      var right_ai = res.data[0].right_ai
      var sgt = res.data[0].sgt
      var else_sgt = res.data[0].else_sgt
      var basic_sick = res.data[0].basic_sick
      var else_sick = res.data[0].else_sick
      this.setData({
        model:model,
        hos_name:hos_name,
        adm_name:adm_name,
        staff_name:staff_name,
        pat_name:pat_name,
        left_type:left_type,
        left_deg:left_deg,
        right_type:right_type,
        right_deg:right_deg,
        left_pic:left_url,
        left_ai:left_ai,
        right_pic:right_url,
        right_ai:right_ai,
        sgt:sgt,
        else_sgt:else_sgt,
        bsc_sck:basic_sick,
        else_sck:else_sick
      })
    })
  }
})