// app.js
App({
  globalData: {
    userInfo: 111,
    openid:111,
    userType: 1,
    // 用户类别 1是医生 0 是 患者
  },

  onLaunch() {
    wx.cloud.init({
      env: 'cloud1-7gn47yx20a65c5c5'  //云开发环境id
    })
    // 获取用户的openid
    wx.cloud.callFunction({
      name:'login'
    }).then(res=>{
      console.log(res)
      console.log(res.result.openid)
      this.globalData.openid = res.result.openid
      // 刚开始启动小程序时，通过openid来查找yuyue-user用户数据库中是否存在用户
      //有的话就不用再登陆
      var flag = 0;
      wx.cloud.database().collection('yuyue-user').where({
        _openid : res.result.openid
      }).get().then(result=>{
        console.log(result.data.length)
        if(result.data.length!=0){
          if(flag == 0){
            console.log(flag)
            wx.cloud.database().collection('Doctor').where({
              Doctorid : res.result.openid
            }).get().then(result=>{
              console.log(result)
              if(result.data.length!=0){
               this.globalData.userInfo = result.data[0]
               this.globalData.userType=1
               wx.switchTab({
                 url: '/pages/homeDoctor/homeDoctor',
                 })
              }else{
                this.globalData.userInfo = res
                this.globalData.userType=0
                wx.switchTab({
                  url: '/pages/homeDoctor/homeDoctor',
                  })
              }
            })
          }
        }
        console.log(flag)
      })
    })
  },
})